package ir.ahmad.myapplication;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.trendyol.medusalib.navigator.MultipleStackNavigator;

public class BaseFragment extends Fragment {
    protected MultipleStackNavigator multipleStackNavigator;
    private Context context;


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        initStackNavigator();
    }

    private void initStackNavigator() {
        if (context instanceof MainActivity && multipleStackNavigator == null) {
            multipleStackNavigator = ((MainActivity) context).multipleStackNavigator;
        }

//        else if (context instanceof MainActivity2 && multipleStackNavigator == null) {
//            multipleStackNavigator = ((MainActivity2) context).multipleStackNavigator;
//        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initStackNavigator();
    }
}
