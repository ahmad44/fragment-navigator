package ir.ahmad.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.View;

import com.trendyol.medusalib.navigator.MultipleStackNavigator;
import com.trendyol.medusalib.navigator.Navigator;
import com.trendyol.medusalib.navigator.NavigatorConfiguration;
import com.trendyol.medusalib.navigator.transaction.NavigatorTransaction;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements Navigator.NavigatorListener {

    public MultipleStackNavigator multipleStackNavigator;

    private List<Fragment> rootFragmentList = new ArrayList<>();
    private AFragment aFragment = AFragment.newInstance("","");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rootFragmentList.add(aFragment);
        
        initNavigator();

//        if (multipleStackNavigator != null)
//            multipleStackNavigator.start(AFragment.newInstance("",""));


    }

    private void initNavigator() {
        multipleStackNavigator = new MultipleStackNavigator(
                getSupportFragmentManager(),
                R.id.flContainer,
                rootFragmentList,
                this,
                new NavigatorConfiguration(0, true, NavigatorTransaction.Companion.getSHOW_HIDE()));
    }

    @Override
    public void onTabChanged(int i) {

    }

    @Override
    public void onBackPressed() {


        if (multipleStackNavigator.canGoBack()) {

//            if (multipleStackNavigator.getCurrentFragment() instanceof FragmentHome) {
//                finish();
//            } else {
//                multipleStackNavigator.goBack();
//            }

            multipleStackNavigator.goBack();

        } else {
            super.onBackPressed();
        }
    }


}
